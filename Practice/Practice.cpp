﻿/**
* @file Practice.cpp
* @author Чорний О.А., гр. 515А
* @date 21.06.2019
* @brief Практична работа
*
* Задание 1
* Проектирование и разработка программы на языке С.Использование Git.*/

#include <stdio.h>
#include <locale.h>

int print_summa(int a, int f); //Вывод в консоль сумму на украинском языке

int main()
{
	setlocale(LC_ALL, "Ukrainian");
	float sum;
	printf("Яку сумму ви хочете перевести на українську мову?  ");
	scanf("%f", &sum);
	if (sum == 0)
	{
		printf("Сумма дорiвнює нулю\n");
		return 0;
	}
	if (sum >= 1000) {
		printf("Задана занадто велика сума\n");
		return 0;
	}
	if (sum < 0) {
		printf("Задана вiд'ємна сума\nЦе неможливо\n");
		return 0;
	}
	printf("%.2f -> ",sum);
	int g, c, f = 0;

	g = sum;
	f = print_summa(g, f);

	c = sum * 100;
	c = c % 100;
	f = print_summa(c, f);

	return 0;
}

int print_summa(int a, int f)
{
	const char *grn[] = { "гривня", "гривнi", "гривень" }; // 1;;; 2 3 4;;; 5 6 7 8 9 0;
	const char *cop[] = { "копiйка", "копiйки", "копiйок" }; // 1;;; 2 3 4;;; 5 6 7 8 9 0;
	const char *ukr1[] = { "", "одна ", "двi ", "три ", "чотири ", "п'ять ",\
		"шiсть ", "сiм ", "вiсiм ", "дев'ять " };
	const char *ukr2[] = { "десять", "одинадацять", "дванадцять", "тринадцять", "чотирнадцять",\
			"п'ятнадцять", "шiстнадцять", "сiмнадцять", "вiсiмнадцять", \
			"дев'ятнадцять" };
	const char *ukr3[] = { "десять", "двадцять", "тридцять", "сорок",\
			"п'ятдесят", "шiстдесят", "сiмдесят", "вiсiмдесят",\
			"дев'яносто", "сто" };
	const char *ukr4[] = { "сто", "двiстi", "триста", "чотириста", "п'ятстот",\
			"шiстсот", "сiмсот", "вiсiмсот", "дев'ятсот" };
	if (a == 0) return 1;
	if (a > 99)
		printf("%s ", ukr4[a / 100 - 1]);
	a = a % 100;
	if (a >= 20)
		printf("%s %s", ukr3[a / 10 - 1], ukr1[a % 10]);
	else
	{
		if (a > 9) printf("%s ", ukr2[a - 10]);
		else printf("%s ", ukr1[a]);
	}
	if (f == 0)
	{
		if ((a >= 10 && a <= 20) || a % 10 == 0 || (a % 10 >= 5 && a % 10 <= 9))
			printf("%s ", grn[2]);
		else if (a % 10 == 1)
			printf("%s ", grn[0]);
		else printf("%s ", grn[1]);
		return 1;
	}
	else
	{
		if ((a >= 10 && a <= 20) || a % 10 == 0 || (a % 10 >= 5 && a % 10 <= 9))
			printf("%s ", cop[2]);
		else if (a % 10 == 1)
			printf("%s ", cop[0]);
		else printf("%s ", cop[1]);
		return 1;
	}
}